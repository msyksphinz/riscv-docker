#
# RISC-V Dockerfile
#

# Pull base image (use Wily for now).
FROM ubuntu:17.10

# Set the maintainer
MAINTAINER msyksphinz

# Install some base tools that we will need to get the risc-v
# toolchain working.
RUN apt update && apt install -y \
  autoconf \
  automake \
  autotools-dev \
  bc \
  bison \
  build-essential \
  curl \
  emacs \
  flex \
  gawk \
  git \
  gperf \
  libmpc-dev \
  libmpfr-dev \
  libgmp-dev \
  libtool \
  ncurses-dev \
  patchutils \
  squashfs-tools \
  texinfo \
  cmake \
  pkg-config \
  libbfd-dev \
  device-tree-compiler \
  zlib1g-dev \
  libz-dev \
  libexpat-dev

# Make a working folder and set the necessary environment variables.
ENV RISCV /riscv
ENV NUMJOBS 4
RUN mkdir -p $RISCV

# Add the GNU utils bin folder to the path.
ENV PATH $RISCV/bin:$PATH

# Obtain the RISCV-tools repo which consists of a number of submodules
# so make sure we get those too.
WORKDIR /opt/riscv
RUN git clone https://github.com/riscv/riscv-tools.git && \
  cd riscv-tools && git submodule update --init --recursive

# Now build the toolchain for RISCV. Set -j 1 to avoid issues on VMs.
WORKDIR /opt/riscv/riscv-tools
ENV MAKEFLAGS="-j4"
RUN ./build.sh
